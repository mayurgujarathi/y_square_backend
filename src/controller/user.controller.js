const userService = require('../service/user.service');
const saltRounds = 10;
const bcrypt = require('bcrypt');
const accessTokenSecret = "xYudnk!7823bm%nVgdBNks1";
const jwt = require('jsonwebtoken');

exports.insertUser = async (req, res) => {
    let response, user = {};

    if (!req.body.username || !req.body.email || !req.body.password) {
        return res.status(400).json({
            status: 400,
            message: 'invalid fields',
            data: null
        });
    }

    const username = await userService.findUserByUsername(req.body.username);
    if (username) {
        return res.status(400).json({
            status: 400,
            message: `username ${req.body.username} is already exists!`,
            data: null
        });  
    }

    const email = await userService.findUserByEmailId(req.body.email);
    if (email) {
        return res.status(400).json({
            status: 400,
            message: `email ${req.body.email} is already exists!`,
            data: null
        });  
    }

    //
    bcrypt.genSalt(saltRounds, function (err, salt) {
        if (err) {
            throw err
        } else {
            bcrypt.hash(req.body.password, salt, async function (err, hash) {
                if (err) {
                    throw err
                } else {
                    user.email = req.body.email,
                    user.username = req.body.username,
                    user.password = hash,
                    user.created_at = new Date(),
                    user.updated_at = new Date()
                }

                console.log('creting user: ', user);

                try {
                    response = await userService.insertUser(user);
                } catch (error) {
                    return res.status(500).json({
                        status: 500,
                        message: error.message,
                        data: null
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: 'Resource added successfully!',
                    data: response.id
                });
            })
        }
    })
    //
    
}

exports.login = async (req, res) => {
    let user;
    console.log('================================>', req.body);
    try {
        user = await userService.findUserByUsernameAndPassword(req.body);
    } catch (error) {
        return res.status(500).json({
            status: 500,
            message: error.message,
            data: null
        });
    }
    
    if (!user) {
        return res.status(400).json({
            status: 400,
            message: 'Bad credentials provided!',
            data: null
        });
    }

    const match = await bcrypt.compare(req.body.password, user.password);
    
    if (!match) {
        return res.status(400).json({
            status: 400,
            message: 'Bad credentials provided!',
            data: null
        });
    }

    res.locals.user = user;

    const accessToken = jwt.sign({
        email_id: user.email,
        user_id: user.id
    }, accessTokenSecret);
    console.log('*****************************user.first_name +  + user.last_name',user);
    return res.status(200).json({
        status: 200,
        message: 'logged in successfully!',
        data: {
            id: user.id,
            accessToken,
            user_role: user.user_role,
            full_name: user.first_name + ' ' + user.last_name
        }
    });
}

exports.getUsers = async (req, res) => {
    const users = await userService.findAllUsers();
    console.log('-------------------->', res.locals.user);
    if (!users || users.length < 0) {
        return res.status(404).send({ 
            status: 404,
            message: "Users was not found!",
            data: users
        });
    }

    return res.status(200).send({ 
        status: 200,
        message: "Records fetched successfully!",
        data: users
    });
}