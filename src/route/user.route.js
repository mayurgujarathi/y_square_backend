const express = require('express')
const router = express.Router()
const userController = require('../controller/user.controller');

module.exports = app => {
    router.post('/register', userController.insertUser);
    router.post('/login', userController.login);
    router.get('/', userController.getUsers);

    app.use('/api/v1/user', router);
}