module.exports = (sequelize, DataTypes) => {
    const UserRole = sequelize.define(
      'UserRole',
      {
        id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
          field: 'id'
        },
        role_name: {
          type: DataTypes.STRING(10),
          allowNull: true,
          field: 'role_name'
        }
      },
      {
        tableName: 'y_square_roles'
      }
    );
    return UserRole;
  };
  