module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'id'
      },
      email: {
        type: DataTypes.STRING(10),
        allowNull: true,
        field: 'email'
      },
      username: {
        type: DataTypes.STRING(20),
        allowNull: true,
        field: 'username'
      },
      password: {
        type: DataTypes.STRING(20),
        allowNull: true,
        field: 'password'
      },
      first_name: {
        type: DataTypes.STRING(20),
        allowNull: true,
        field: 'first_name'
      },
      last_name: {
        type: DataTypes.STRING(20),
        allowNull: true,
        field: 'last_name'
      },
      role_id: {
        type: DataTypes.STRING(20),
        allowNull: true,
        field: 'role_id',
        references: {
          tableName: 'y_square_roles',
          key: 'id'
        }
      },
      is_online: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: 'is_online'
      },
      socket_id: {
        type: DataTypes.STRING(50),
        allowNull: true,
        field: 'socket_id'
      }
    },
    {
      tableName: 'y_square_users'
    }
  );
  User.associate = models => {
    User.hasOne(models.UserRole, { sourceKey: 'role_id', foreignKey: 'id', as: 'user_role' });
  };
  return User;
};
