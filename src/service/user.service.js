const User = require('../model/index').User;
const UserRole = require('../model/index').UserRole;

exports.insertUser = async (user) => {
    console.log(user);
    return await User.create(user);
}

exports.findUserByUsernameAndPassword = async (user) => {
    console.log(user);
    return await User.findOne({
        where: {
            username: user.username
        },
        include: [
            {
                model: UserRole,
                as: 'user_role'
            }
        ]
    });
}

exports.findUserByEmailId = async (email) => {
    return await User.findOne({
        where: {
            email: email
        }
    });
}

exports.findUserByUsername = async (username) => {
    return await User.findOne({
        where: {
            username: username
        }
    });
}

exports.findAllUsers = async () => {
    console.log('in service');
    return await User.findAll({
        attributes: ['id', 'socket_id', 'is_online', 'username', 'first_name', 'last_name']
    });
}

exports.updateSocketStatusBySocketId = async (payload) => {
    const result = await User.update({
        is_online: 0,
    }, {
        where: { socket_id: payload.socketId }
    });

    await this.findAllUsers();
}

exports.updateUserSocketStatusById = async (payload) => {
    console.log('in service', payload);
    const result = await User.update({
        is_online: payload.status,
        socket_id: payload.socketId
    }, {
        where: { id: payload.userId }
    });
    return await this.findAllUsers();

}

exports.findOnlineUsers = async () => {
    return await User.findAll({
        where: { is_online: 1 },
        attributes: ['id', 'is_online']
    });
}