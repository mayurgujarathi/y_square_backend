const userService = require('../service/user.service');
const ONLINE_STATUS = 1;
const OFFLINE_STATUS = 0;

const activate = async io => {
  console.log(`========== CONNECTION =============${io}`);
  io.on('connection', async socket => {

    if (typeof socket.handshake.query.user_id !== 'undefined' && socket.handshake.query.user_id && socket.handshake.query.user_id.trim() !== 'null') {
      const userConnectionStatus = {
        socketId: socket.id,
        userId: parseInt(socket.handshake.query.user_id, 0),
        status: ONLINE_STATUS
      };
      const activeUsers = await userService.updateUserSocketStatusById(userConnectionStatus);
      io.emit('active_users', { data: activeUsers })
      const onlineUsers = await userService.findOnlineUsers(userConnectionStatus);
      io.emit('refresh_user_list', { data: onlineUsers })
    }
    socket.on('send_notification', async data => {
      io.to(data.socketId).emit('receive_notification', { message: 'Message from ADMIN' });
    });

    socket.on('disconnect', async () => {
      const activeUsers = await userService.updateSocketStatusBySocketId({ socketId: socket.id });
      const onlineUsers = await userService.findOnlineUsers();
      io.emit('refresh_user_list', { data: onlineUsers })
    });
  });
};

module.exports = { activate };
