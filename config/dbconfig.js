module.exports = {
    userName: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DATABASE,
    host: process.env.HOST,
    dialect: process.env.DIALECT,
    define: {
      underscored: true
    },
    pool: {
      max: 5,
      idle: process.env.DB_POOL_IDLE,
      acquire: process.env.DB_POOL_ACQUIRE
    }
  };
  