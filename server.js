const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const socketio = require('socket.io');
const socket = require('./src/shared/socketio');
const http = require('http');

global.__basedir = __dirname;

const app = express()
const corsOptions = {
	'https://test-app.org': true,
	'http://localhost:4200': true,
	'http://localhost:8080': true
}

app.use(cors(corsOptions));

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization')
	res.setHeader('Access-Control-Allow-Credentials', true)
	next()
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html');
})

app.use((error, req, res, next) => {
	res.status(500).json(error.message);
	next(error);
});

const httpserver = http.Server(app);
const PORT = process.env.PORT || 8080
httpserver.listen(PORT, () => {
  console.log(`Node App started on port: ${process.env.PORT}`);
});

io = socketio(httpserver);
socket.activate(io);

require('./src/route/user.route')(app)